const gulp = require("gulp");
const del = require("del");
const sass = require("gulp-sass");
const cssnano = require("gulp-cssnano");
const imagemin = require("gulp-imagemin");
const gulpif = require("gulp-if");
const rename = require("gulp-rename");
const sourcemaps = require("gulp-sourcemaps");
const connect = require("gulp-connect");
const autoprefixer = require("gulp-autoprefixer");
const spritesmith = require("gulp.spritesmith");
const nunjucksRender = require("gulp-nunjucks-render");
const data = require("gulp-data");

const { rollup } = require("rollup");
const { terser } = require("rollup-plugin-terser");
const resolve = require("rollup-plugin-node-resolve");
const commonjs = require("rollup-plugin-commonjs");
const babel = require("rollup-plugin-babel");

const isProduction = process.env.NODE_ENV === "production";

sass.compiler = require("node-sass");



const paths = {
	html: {
		src: "src/templates/*.html",
		subTemplates: "src/templates/sub-templates/*.html",
		subTemplatesDest: "build/sub-templates/",
		watch: "src/templates/**/*",
		dest: "build/"
	},

	sprite: {
		src: "src/arquivos/sprites/**/*.{png,jpg}",
		dest: "build/arquivos/",
		destCss: "src/arquivos/scss/lib"
	},

	styles: {
		src: "src/arquivos/scss/*.scss",
		watch: "src/arquivos/scss/**/*.scss",
		dest: "build/arquivos"
	},

	scripts: {
		src: ["src/arquivos/js/main.js"],
		watch: "src/arquivos/js/**/*.js",
		dest: "build/arquivos/bundle.js"
	},

	img: {
		src: "src/arquivos/img/**/*.{png,gif,jpg}",
		dest: "build/arquivos"
	},

	root: "build"
};

function html() {
	const process = gulp
		.src(paths.html.src)
		.pipe(data({ prod: isProduction }))
		.pipe(
			nunjucksRender({
				path: "src/templates"
			})
		)
		.pipe(gulp.dest("build"))
		.pipe(connect.reload());
	if (isProduction) {
		const process2 = gulp
			.src(paths.html.subTemplates)
			.pipe(gulp.dest(paths.html.subTemplatesDest));
	}

	return process;
}

function clean() {
	return del([paths.root]);
}

async function scripts() {
	const bundle = await rollup({
		input: paths.scripts.src,
		plugins: [
			resolve(),
			commonjs(),
			babel({
				exclude: "node_modules/**",
				babelrc: false,
				runtimeHelpers: true,
				plugins: ["@babel/plugin-transform-runtime"],
				presets: [["@babel/env", { modules: false }]]
			}),
			isProduction && terser()
		],
		external: ["jquery"]
	});

	const outputConfig = {
		file: paths.scripts.dest,
		sourcemap: false,
		format: "iife",
		globals: {
			jquery: "$"
		}
	};

	if (isProduction) {
		return bundle.write(outputConfig);
	}

	outputConfig.sourcemap = true;
	return bundle.write(outputConfig);
}

function sprite() {
	return gulp
		.src(paths.sprite.src)
		.pipe(
			spritesmith({
				imgName: `sprite.png`,
				cssName: "_sprite.scss",
				padding: 25
			})
		)
		.pipe(
			gulpif(
				"*.png",
				gulp.dest(paths.sprite.dest),
				gulp.dest(paths.sprite.destCss)
			)
		);
}

function styles() {
	return gulp
		.src(paths.styles.src)
		.pipe(gulpif(!isProduction, sourcemaps.init()))
		.pipe(sass().on("error", sass.logError))
		.pipe(autoprefixer())
		.pipe(gulpif(isProduction, cssnano()))
		.pipe(
			rename({
				extname: ".css"
			})
		)
		.pipe(gulpif(!isProduction, sourcemaps.write(".")))
		.pipe(gulp.dest(paths.styles.dest))
		.pipe(connect.reload());
}

function img() {
	return gulp
		.src(paths.img.src)
		.pipe(imagemin())
		.pipe(gulp.dest(paths.img.dest))
		.pipe(connect.reload());
}

function devServer() {
	connect.server({
		root: paths.root,
		livereload: true,
		port: 3000
	});
}

function watch() {
	devServer();
	build();
	gulp.watch(paths.scripts.watch, { ignoreInitial: false }, scripts);
	gulp.watch(paths.styles.watch, { ignoreInitial: false }, styles);
	gulp.watch(paths.img.src, { ignoreInitial: false }, img);
	gulp.watch(paths.html.watch, { ignoreInitial: false }, html);
}

const build = gulp.series(
	clean,
	sprite,
	gulp.parallel(html, scripts, styles, img)
);

exports.build = build;
exports.html = html;
exports.devServer = devServer;
exports.img = img;
exports.clean = clean;
exports.styles = styles;
exports.watch = watch;
exports.sprite = sprite;
exports.scripts = scripts;
